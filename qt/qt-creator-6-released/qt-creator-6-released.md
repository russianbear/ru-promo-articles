# Qt Creator 6 Released

We are happy to announce the release of Qt Creator 6!

Here is a selection of changes and improvements that we did in Qt Creator 6. Please have a look at our [change log](https://code.qt.io/cgit/qt-creator/qt-creator.git/tree/dist/changes-6.0.0.md?h=6.0) for more details.

## General

* Our prebuilt binaries of Qt Creator 6 are now based on Qt 6.2!

* We provide universal Intel+ARM binaries for macOS.

* We moved the launching of external processes, like the build tools and clang-tidy and other tools, to a separate server process. This avoids issues on Linux, where forking a process from a large application is more expensive than from a small server process.

## Editing

* Our text editor now supports general multi cursor editing (add cursors with Alt+Click).[![FirstTry](https://www.qt.io/hs-fs/hubfs/FirstTry.gif?width=320&name=FirstTry.gif)](https://code.qt.io/cgit/qt-creator/qt-creator.git/tree/dist/changes-6.0.0.md?h=6.0)

* The C++ code model was upgraded to LLVM 13.

* Editing C++ with clangd is now fully supported, but still disabled by default. Enable it in the options, *C++* > *Clangd*. For details check our separate blog post about [clangd support in Qt Creator](/blog/qt-creator-and-clangd-an-introduction?hsLang=en).

* The integrated Qt Quick Designer is now disabled by default. Qt Creator will open `.ui.qml` files in Qt Design Studio. This is a step towards a more integrated workflow between Qt Design Studio and Qt Creator ([video](https://www.youtube.com/watch?v=o3ESfuBOaiI)). Qt Quick Designer is still there, you can manually enable it again by checking the QmlDesigner plugin in Help > About Plugins.  

## Projects

* We added *Show in File System View* to the Projects tree's context menu.

* We added a global search for *Files in All Project Directories* to advanced find, similar to the Locator filter.

* For CMake projects we removed the special Headers nodes and in turn improved the way our CMake support handles header files that are mentioned in target sources. The preferred way is to add headers to target sources, which helps Qt Creator and other tools like Clazy to do the right thing. The *File System* view and *Files in All Project Directories* Locator filter and advanced search can be used in other cases. For details check out the [Qt Creator 6 — CMake update](/blog/qt-creator-6-cmake-update?hsLang=en) blog post.

* The support for building and running in Docker containers makes progress. More and more places in Qt Creator internally accept remote file paths. The experimental support is now also available to Windows users.

## Get Qt Creator 6

The open-source version is available on the [Qt download page under «Qt Creator»](/offline-installers?hsLang=en), and you find commercially licensed packages on the [Qt Account Portal](https://login.qt.io/). Qt Creator 6 is also available as an update in the online installer. Please post issues in our [bug tracker](https://bugreports.qt.io/projects/QTCREATORBUG). You can also find us on IRC on #qt-creator on [irc.libera.chat](https://web.libera.chat/), and on the [Qt Creator mailing list](http://lists.qt-project.org/mailman/listinfo/qt-creator).

You can read the Qt Creator Manual in Qt Creator in the [Help mode](https://doc-snapshots.qt.io/qtcreator-master/creator-help.html) or access it online in the [Qt documentation portal](https://doc-snapshots.qt.io/qtcreator-master/index.html).

_Автор:_ David Schulz  
_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Источник:_ https://www.qt.io/blog/qt-creator-6-released  

<!-- 💡: bug tracker → система отслеживания ошибок -->
<!-- 💡: directories → каталоги -->
