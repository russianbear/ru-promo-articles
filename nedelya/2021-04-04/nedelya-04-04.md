# This week in KDE: KHamburgerMenu and some good bugfixes

> 28 марта – 4 апреля, основное — прим. переводчика

Today I’d like to introduce an interesting new component that will eventually be rolled out in many KDE apps with menubars: [KHamburgerMenu](https://invent.kde.org/frameworks/kconfigwidgets/-/merge_requests/25). This re-usable component allows QWidgets-based apps to show a custom hamburger menu when the main menubar is hidden, like Dolphin already does. In fact, we are in the process of [porting Dolphin’s custom implementation](https://invent.kde.org/system/dolphin/-/merge_requests/168) to use KHamburgerMenu. Here’s a sneak peek of the early alpha work:

![0](https://pointieststick.files.wordpress.com/2021/04/khamburgermenu-in-dolphin.jpeg?w=1024)

NOTE: this layout is not final and is highly likely to change! Adopting KHamburgerMenu offers KDE apps many features and advantages:

<!-- НЕ НУЖНО ДОСЛОВНО, СОКРАЩАЙ! -->
1. It offers an escape valve for users who hide the menubar by accident! This is a surprisingly serious problem, and KDE’s bugtracker is littered with reports from users who did not figure out how to show their menubar again!
2. It allows users who want maximum vertical space to choose to hide the menubar without losing GUI access to the menu items, or having to activate the titlebar-based menubar or global menubar (maybe they don’t like those; we’re all about choice in KDE after all!).
3. Being a re-usable framework, KHamburgerMenu can be applied to KDE apps besides just Dolphin.
4. Though the hamburger menu shows a curated assortment of items from the main menubar, the app’s full menu structure is available from a sub-menu on the bottom (See «For 83 more actions:», in the screenshot above) so nothing is ever completely hidden. This means that the curation of items can be more selective because there is no longer a need to cram everything into the main level to make sure it isn’t totally invisible to the user. This will result in hamburger menus whose contents are short and relevant.
5. The new hamburger button is just a regular toolbar item like all others, so you can relocate it, rename it, change its icon, or remove it entirely if you really do want to hide the menubar and not use a hamburger menu.

Big thanks to Felix Ernst for this work! It has been merged for Frameworks 5.81, and we are working on porting Dolphin and Gwenview to use it, hopefully to land in their 20.08 releases. And hopefully more will be coming too.

Some of you may be wondering, «Does KDE plan to kill off the menubar the way GNOME did? Have your feelings on this matter changed since you wrote [a scathing criticism of headerbars and hamburger menus back in 2018](https://pointieststick.com/2018/12/18/on-headerbars/)?» My answers would be 1) no, we have no intention of killing off traditional menubars especially for large and complex apps, and 2) yes, my personal feelings on Hamburger menus have changed somewhat over time, particularly because KHamburgerMenu is engineered to avoid what we see as the drawbacks of the GNOME-style hamburger menu. However, I’ll have to expand on that in another blog post because this intro section is getting waaaaay too long! So stay tuned.

Anyway, here’s the rest:

## Bugfixes & Performance Improvements

The FortiSSLVPN NetworkManager plugin [now works](https://bugs.kde.org/show_bug.cgi?id=434940) (Pedro Gomes, Plasma 5.21.4)

Single-keyboard-layout setups in the Plasma Wayland session [no longer fail to load your keyboard options and variants](https://bugs.kde.org/show_bug.cgi?id=433576) (e.g. alternative Caps Lock behaviors) (Андрей Бутырский, Plasma 5.21.4)

Task Manager entries for apps capable of showing a numbered badge in the corner [no longer sometimes pointlessly show a badge with the number zero in it](https://bugs.kde.org/show_bug.cgi?id=428059) (Bharadwaj Raju, Plasma 5.21.4)

You can once again [create new files on a writable FTP share using Dolphin’s «Create New…» menu](https://bugs.kde.org/show_bug.cgi?id=429541) (Méven Car, Frameworks 5.81)

The image thumbnailer [no longer sometimes crashes when taking a screenshot](https://bugs.kde.org/show_bug.cgi?id=430862) (Méven Car, Frameworks 5.81)

The Baloo file indexer [is now more reliably able to notice when files have been renamed or moved](https://bugs.kde.org/show_bug.cgi?id=433116) (Stefan Brüns, Frameworks 5.81)

## User Interface Improvements

Taking into account user feedback from last week regarding the new Plasma applet config dialog appearance, [we decided to revert the changes and go back to the customary KDE style](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/401) in the absence of consensus and work to simultaneously change **all** settings dialogs to use a new style (Marco Martin, Plasma 5.22):

![1](https://pointieststick.files.wordpress.com/2021/04/screenshot_20210401_083135.png?w=1024)

Search fields in [Discover](https://invent.kde.org/plasma/discover/-/merge_requests/100) and [the «Get New [thing]» dialogs](https://bugs.kde.org/show_bug.cgi?id=435084) now automatically initiate a search a few seconds after you finish typing, if you haven’t already pressed the Enter key by that point (Dan Leinir Turthra Jensen and Nate Graham, Frameworks 5.81 and Plasma 5.22)

When using our Breeze GTK theme, scrollbars in ancient GTK2 apps [now look more similar to Breeze-style scrollbars in Qt apps](https://invent.kde.org/plasma/breeze-gtk/-/merge_requests/19) (Marco Rebhan, Plasma 5.22)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Максим Маршев (PeWpIC)](https://t.me/PeWpIC)  
_Источник:_ https://pointieststick.com/2021/04/02/this-week-in-kde-khamburgermenu-and-some-good-bugfixes/  

<!-- 💡: applet → виджет -->
<!-- 💡: bugtracker → система отслеживания ошибок -->
<!-- 💡: get new [thing] → «Загрузить [что-то]» -->
<!-- 💡: headerbar → панель заголовка -->
<!-- 💡: icon → значок -->
<!-- 💡: location → расположение -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
