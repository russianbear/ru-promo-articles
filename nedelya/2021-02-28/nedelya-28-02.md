# Эта неделя в KDE: всего понемногу

> 21–28 февраля, основное — прим. переводчика

Продолжаем исправлять Plasma 5.21, и мы также много чего довели до ума в интерфейсе:

## Новые возможности

Теперь Вы [можете применять глобальные темы, цветовые схемы, темы курсора, темы Plasma и обои из командной строки, используя некоторые новые утилиты командной строки с именами вроде `plasma-apply-colorscheme`](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/630) (Dan Leinir Turthra Jensen, Plasma 5.22)

Приложения KDE [теперь поддерживают форматы изображений HEIF и HEIC](https://invent.kde.org/frameworks/kimageformats/-/merge_requests/16) (Daniel Novomeský, Frameworks 5.80)

Панель задач [может быть настроена таким образом, чтобы скрытая панель не становилась видимой, когда одно или нескольких приложений получает статус «требует внимания»](https://bugs.kde.org/show_bug.cgi?id=394119) (Michael Moon, Plasma 5.22)

## Исправления и улучшения производительности

Установленный kio-fuse [больше не ломает возможность KRunner отображать адреса man: и info: в вашем веб-браузере](https://bugs.kde.org/show_bug.cgi?id=433300) (Fabian Vogt, kio-fuse 5.0.1)

Менеджер окон KWin [больше не ломает сессию Plasma Wayland при копировании чего-либо в приложении XWayland и немедленной вставке в нативное приложение Wayland](https://invent.kde.org/plasma/kwin/-/merge_requests/693) (Jan Blackquill, Plasma 5.21.1)

Plasma [больше не падает при закрытии виджета громкости](https://bugs.kde.org/show_bug.cgi?id=432482) (David Edmundson, Plasma 5.21.1)

Прокрутка полностью до конца в любом списке приложений в Discover [больше не вызывает появления ложного индикатора загрузки внизу](https://bugs.kde.org/show_bug.cgi?id=432384) (Aleix Pol Gonzalez, Plasma 5.21.1)

Повтор клавиш [уже наконец-то действительно точно включён по умолчанию](https://bugs.kde.org/show_bug.cgi?id=431923). Извините за это. 😦 (Jan Blackquill и David Edmundson, Plasma 5.21.2)

Экраны на странице их настройки в Параметрах системы [снова перетаскиваются](https://bugs.kde.org/show_bug.cgi?id=433178) (Marco Martin, Plasma 5.21.2)

На странице значков Параметров системы [нижний ряд кнопок теперь перемещает не уместившиеся в свободное место кнопки во всплывающее меню](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/665), что особенно полезно в Plasma Mobile (Dan Leinir Turthra Jensen, Plasma 5.21.2):

<https://i.imgur.com/h0VyVh0.mp4>

Значки системного лотка в очень тонких панелях [больше не должны быть слегка мыльными](https://bugs.kde.org/show_bug.cgi?id=433138) (Niccolò Venerandi, Plasma 5.21.2)

Виртуальные клавиатуры [больше не закрывают панели Plasma, когда они видимы](https://invent.kde.org/plasma/kwin/-/merge_requests/717) (Aleix Pol Gonzalez, Plasma 5.22)

Переименование файла изображения таким же именем, как было у другого переименованного файла, [больше не вызывает отображение неправильной миниатюры у нового переименованного файла](https://bugs.kde.org/show_bug.cgi?id=433127) (Méven Car, Frameworks 5.80)

## Улучшение пользовательского интерфейса

Функция переформатирования текста Konsole [теперь работает лучше для пользователей оболочки zsh](https://invent.kde.org/utilities/konsole/-/merge_requests/337) (Carlos Alves, Konsole 21.04)

Dolphin [теперь чуть быстрее отображает миниатюры](https://invent.kde.org/system/dolphin/-/merge_requests/182) (Méven Car, Dolphin 21.04)

Панель заголовка у GTK-приложений теперь отображает кнопки свернуть/развернуть/и т.д. [даже при использовании темы декорации окон Aurorae](https://bugs.kde.org/show_bug.cgi?id=432712) (Alois Wohlschlager, Plasma 5.21.2)

Поиск Discover для приложений Flatpak [теперь сильнее основывается на соответствие заголовку и более высоких рейтингах](https://invent.kde.org/plasma/discover/commit/28cc7c56471bd11b84e0da639a9dd33cfbcaf3b9) (Aleix Pol Gonzales, Plasma 5.22):

![1](https://pointieststick.files.wordpress.com/2021/02/screenshot_20210224_195511.png?w=881)

Discover [теперь понятнее показывает в поиске и списках, когда приложение приходит с нестандартного бэкенда](https://bugs.kde.org/show_bug.cgi?id=433370) (Aleix Pol Gonzalez и Nate Graham, Plasma 5.22):

![2](https://pointieststick.files.wordpress.com/2021/02/screenshot_20210224_104946.png?w=1003)

Компактный/мобильный вид Discover [теперь понятнее показывает на домашней странице, что есть обновления](https://invent.kde.org/plasma/discover/-/merge_requests/82) (Dan Leinir Turthra Jensen, Plasma 5.22):

![3](https://pointieststick.files.wordpress.com/2021/02/image.png?w=744)

Уведомления, информирующие вас о файловой операции с известным адресом назначения, [теперь всегда отображают кнопку «открыть содержащую папку»](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/672) (Kai Uwe Broulik, Plasma 5.22)

При сворачивании боковой панели в приложении Kirigami, содержимое панели инструментов этого приложения [движется как по маслу](https://bugs.kde.org/show_bug.cgi?id=413841) (Marco Martin, Frameworks 5.80)

## Сетевое присутствие

Carl Schwan переделал онлайн-документацию api.kde.org и Kirigami: <https://carlschwan.eu/2021/02/26/documentation-improvements-in-kde/>

Niccolò Venerandi опубликовал третье видео в серии о создании тем Plasma:
<https://www.youtube.com/watch?v=5HGdJ8ZhXos>

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [boingo-00](https://t.me/boingo00)  
_Источник:_ https://pointieststick.com/2021/02/26/this-week-in-kde-a-little-bit-of-everything-2/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить [что-то]» -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
