# На этой неделе в KDE: обзор принтеров Samba и другое

> 19–26 декабря, основное — прим. переводчика

С наступающим Новым годом! 🎅 Мы со своей стороны дарим подарки всем, кто хорошо вёл себя в уходящем году:

## Новые возможности

[Samba printer browsing](https://bugs.kde.org/show_bug.cgi?id=368305)! (Harald Sitter, print-manager 22.04):

![0](https://pointieststick.files.wordpress.com/2021/12/samba-printer-browsing.png)

*Before you ask, yes, this UI is pretty dated. It will eventually be re-done as a part of the ongoing port of all System Settings pages to Qt Quick.*

The «Cover Switch» and «Flip Switch» effects [are now back](https://bugs.kde.org/show_bug.cgi?id=443757), newly written in QML for easier extensibility in the future! (Ismael Asensio, Plasma 5.24)

![2](https://pointieststick.files.wordpress.com/2021/12/qml_flipswitch.png)

*The Desktop Cube effect will be next, and hopefully should show up in Plasma 5.25!*

## Исправления ошибок и улучшения производительности

Yakuake’s window [is now faster to appear](https://invent.kde.org/utilities/yakuake/-/merge_requests/51) (Jan Blackquill, Yakuake 21.12.1)

In the Plasma Wayland session, Yakuake [no longer appears underneath a top panel](https://bugs.kde.org/show_bug.cgi?id=408468) (Tranter Madi, Yakuake 22.04)

Partition Manager [no longer keeps asking for authentication over and over again if you cancel the authentication prompt, and instead shows you a friendly message indicating what the problem is and how you can fix it](https://bugs.kde.org/show_bug.cgi?id=428974) (Alessio Bonfiglio, Partition Manager 22.04):

![1](https://pointieststick.files.wordpress.com/2021/12/message.png)

Fixed [a memory leak in Notifications](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1303) (David Edmundson, Plasma 5.18.9)

The Digital Clock’s calendar view [now always shows the right colors when using the Breeze Light Plasma theme, or any other theme that has hardcoded light colors](https://bugs.kde.org/show_bug.cgi?id=446991) (Noah Davis, Plasma 5.23.5)

Plasma [now shuts down faster by no longer accepting new connections after it’s begun the shutdown process, which particularly helps when using KDE Connect](https://bugs.kde.org/show_bug.cgi?id=432643) (Tomasz Lemeich, Plasma 5.24)

The new «Set as Wallpaper» context menu item [now only changes the wallpapers of the desktops in the current Activity, not all Activities](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1293) (Fushan Wen, Plasma 5.24)

The link modification UI of the Properties dialog [now shows the correct information in the correct places](https://invent.kde.org/frameworks/kio/-/merge_requests/572) (Алексей Никифоров, 5.90)

## Улучшения пользовательского интерфейса

The Desktop context menu’s [«Open in Dolphin» item](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1237) has been [replaced with «Configure Display Settings»](https://bugs.kde.org/show_bug.cgi?id=355679) by default (Ezike Ebuka and Nate Graham, Plasma 5.24):

![3](https://pointieststick.files.wordpress.com/2021/12/configure-display-settings.png)

*And don’t forget that this menu is configurable (in Configure Desktop and Wallpaper > Mouse Actions > Right-Button > «Configure»), so you can remove stuff from it yourself that you never use!*

You [can now drag a panel from anywhere on its Edit Mode toolbar, not just from a tiny button. And this is now more obvious with the addition of a label that indicates it](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/764) (Björn Feber, Plasma 5.24):

![4](https://pointieststick.files.wordpress.com/2021/12/drag-to-move.png)

System Settings pages that display a single big grid or list [now have a more modern frameless style](https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/91) (Nate Graham, Frameworks 5.90)

* ![6](https://pointieststick.files.wordpress.com/2021/12/cursors.png)

* ![7](https://pointieststick.files.wordpress.com/2021/12/background_services.png)

Toolbar buttons that you can click-and-hold to show a menu [will now also show that menu when you right-click them](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/86) (Kai Uwe Broulik, Frameworks 5.90)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Источник:_ https://pointieststick.com/2021/12/25/this-week-in-kde-samba-printer-browsing-and-more/  

<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Settings → Параметры системы -->
