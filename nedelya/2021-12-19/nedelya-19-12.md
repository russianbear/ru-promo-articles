# На этой неделе в KDE: kind of everything

> 12–19 декабря, основное — прим. переводчика

Today’s post should have something in it for everyone! Early holiday gifts!

## Новые возможности

You [can now change your wallpaper to any image using its context menu!](https://bugs.kde.org/show_bug.cgi?id=358038) (Fushan Wen, Plasma 5.24):

![0](https://pointieststick.files.wordpress.com/2021/12/screenshot_20211213_133907.png)

*Don’t worry, it shows up in Dolphin’s context menu too!*

In the Plasma Wayland session, [there is now a barebones drawing tablet page in System Settings](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/677). It doesn’t have much in it right now, but more will be added over time (Aleix Pol Gonzalez, Plasma 5.24)

It’s [now possible for Global Themes to specify and change Latte Dock layouts](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1243) (Michail Vourlakos, Plasma 5.24)

## Wayland

In the Wayland session, the setting to make Spectacle automatically copy a just-taken screenshot to the clipboard when invoked using global keyboard shortcuts [now works](https://bugs.kde.org/show_bug.cgi?id=429390) (Méven Car, Spectacle 22.04)

In the Wayland session, mouse and touchpad settings to let you toggle between «Flat» and «Adaptive» acceleration profiles [now work](https://bugs.kde.org/show_bug.cgi?id=427771) (Arjen Hiemstra, Plasma 5.23.5)

In the Wayland session, applying a «No titlebar and frame» window rule [no longer makes the window become super tiny](https://bugs.kde.org/show_bug.cgi?id=445140) (Ismael Asensio, Plasma 5.23.5)

In the Wayland session, [various Chromium-based web browsers now show their windows properly](https://bugs.kde.org/show_bug.cgi?id=445259) (Влад Загородний, Plasma 5.24)

In the Wayland session, [you can now use the default Meta+Tab shortcut to cycle through more than two activities at a time](https://bugs.kde.org/show_bug.cgi?id=397662) (David Redondo, Plasma 5.24)

In the Wayland session, [drag-and-drop now works on FreeBSD](https://invent.kde.org/plasma/kwayland-server/-/merge_requests/333) (Влад Загородний, Plasma 5.24)

## Исправления ошибок и улучшения производительности

System Settings [no longer sometimes crashes when you try to install or update Global Themes](https://bugs.kde.org/show_bug.cgi?id=439797) (David Edmundson, Plasma 5.23.5)

Scrolling over Gwenview’s zoom combobox to change the zoom level [now works more predictably and reliably](https://invent.kde.org/graphics/gwenview/-/merge_requests/112) (Felix Ernst, Gwenview 21.12.1)

Fixed [a memory leak in the thumbnail preview generator](https://invent.kde.org/network/kio-extras/-/merge_requests/136) (Waqar Ahmed, kio-extras 22.04)

Konsole’s scrolling performance [is now 2x faster](https://invent.kde.org/utilities/konsole/-/merge_requests/555)! (Waqar Ahmed, Konsole 22.04)

[Fixed a variety of memory leaks that could cause KWin to crash when opening various 3rd-party apps or the new Overview effect](https://invent.kde.org/plasma/kwin/-/merge_requests/1791) (Влад Загородний, Plasma 5.23.5)

The application launcher [no longer fails to search properly when there are multiple instances of it](https://bugs.kde.org/show_bug.cgi?id=443131) (Noah Davis, Plasma 5.23.5)

The Task Manager context menu’s «Show in all Activities» item [once again works](https://bugs.kde.org/show_bug.cgi?id=440496) (Fushan Wen, Plasma 5.24)

Rotating a monitor that’s displaying full-screen content [now causes the content to be re-laid-out properly](https://invent.kde.org/plasma/kwin/-/merge_requests/1789) (Jiya Dong, Plasma 5.24)

Merged [the first of many upcoming multi-screen fixes](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1242), which should help with panels and desktops becoming mixed up when screens are removed and re-attached (Marco Martin, Plasma 5.24)

Linked buttons in GTK apps styled with the Breeze GTK theme [now have a raised and conjoined appearance](https://bugs.kde.org/show_bug.cgi?id=446206), so you can tell they’re linked (Jan Blackquill, Plasma 5.24):

![1](https://pointieststick.files.wordpress.com/2021/12/linked-buttons.png)

Certain tooltips in Plasma applets [no longer display visual glitches in the corners when using the Breeze Plasma theme](https://bugs.kde.org/show_bug.cgi?id=442745) (Noah Davis, Frameworks 5.90):

![2](https://pointieststick.files.wordpress.com/2021/12/no-glitch.png)

## Улучшения пользовательского интерфейса

When you try to use Gwenview’s camera importer without the support package that it requires, [it now detects this and guides you through installing it](https://bugs.kde.org/show_bug.cgi?id=446420) (Fushan Wen, Gwenview 22.04):

![3](https://pointieststick.files.wordpress.com/2021/12/gwenview-importer-message.png)

When using the systemwide double-click setting, [in Dolphin you can now ctrl-double-click on a folder to open it in a new tab, and shift-double-click on a folder to open it in a new window](https://invent.kde.org/system/dolphin/-/merge_requests/286) (Alessio Bonfiglio, Dolphin 22.04)

Discover [now lets you open and install locally-downloaded Flatpak apps from repos not active on the system, and tells you that installing them will add their repo](https://bugs.kde.org/show_bug.cgi?id=445596) (Aleix Pol Gonzalez, Plasma 5.24):

![4](https://pointieststick.files.wordpress.com/2021/12/discover-flatpak-message.png)

*The excessively large bottom padding in that message is a known bug that will eventually be fixed*

It’s [now possible to open Info Center via a button on the «About this System» page in System Settings](https://bugs.kde.org/show_bug.cgi?id=438213) (Harald Sitter, Plasma 5.24):

![5](https://pointieststick.files.wordpress.com/2021/12/more-info.png)

Uploading an image to Imgur [now displays the result using a system notification](https://bugs.kde.org/show_bug.cgi?id=437347) and also [now shows you the delete link](https://bugs.kde.org/show_bug.cgi?id=394181), so you can remove the uploaded image if you uploaded something you didn’t mean to or regret (Nicolas Fella, Frameworks 5.90):

![7](https://pointieststick.files.wordpress.com/2021/12/cool-stuff.png)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Андреев](https://vk.com/ilyandl)  
_Источник:_ https://pointieststick.com/2021/12/17/this-week-in-kde-kind-of-everything/  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
