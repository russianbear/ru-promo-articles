# This week in KDE: Elisa grows up

> 7–14 марта, основное — прим. переводчика

This week I want to highlight something big: Elisa [now has a full mobile interface, making it a first-class citizen on Plasma Mobile and Android](https://invent.kde.org/multimedia/elisa/-/merge_requests/205)! A ton of the code is shared, thanks to KDE’s Kirigami user interface toolkit. Thanks very much to Devin Lin for this enormously impactful contribution that makes Elisa a fully convergent music player!

![1](https://pointieststick.files.wordpress.com/2021/03/tracks.png?w=472)
![2](https://pointieststick.files.wordpress.com/2021/03/playlist.png?w=460)
![3](https://pointieststick.files.wordpress.com/2021/03/player.png?w=472)

In addition, we got a lot of work done on the new Plasma System monitor app, which is going to replace the venerable KSysGuard by default in Plasma 5.22. It’s currently optional, so we’re very thankful for the amount of testing that adventurous people have done with it. This promises to make the full roll-out in Plasma 5.22 much smoother. Thank you everyone!

## Other New Features

Kate now has a feature to [let you jump back to the previous cursor position](https://invent.kde.org/utilities/kate/-/merge_requests/306) (Waqar Ahmed, Kate 21.04):

![4](https://pointieststick.files.wordpress.com/2021/03/screenshot_20210310_092614.png?w=1024)

You are now [notified and asked to back up your data when problems with your disk are detected that do not quite rise to the level of a full S.M.A.R.T failure, but are nonetheless worrying and could potentially cause data loss soon](https://bugs.kde.org/show_bug.cgi?id=429804) (Harald Sitter, Plasma 5.22)

## Bugfixes & Performance Improvements

When Dolphin is sorting by on-disk sizes, [it no longer orders folders first when not asked to do so](https://bugs.kde.org/show_bug.cgi?id=433207) (Méven Car, Dolphin 21.04)

Spectacle [no longer inappropriately enables «on click» mode under certain circumstances](https://invent.kde.org/graphics/spectacle/-/merge_requests/57) (Влад Загородний, Spectacle 21.04)

Fixed a case where the new Plasma System Monitor app [could crash on Wayland](https://bugs.kde.org/show_bug.cgi?id=434074) (Arjen Hiemstra, Plasma 5.21.3)

The «Force Font DPI» setting [is once again usable on Wayland](https://bugs.kde.org/show_bug.cgi?id=433115) (Nate Graham, Plasma 5.21.3)

Moving a widget within a page in the new Plasma System Monitor app [now works](https://bugs.kde.org/show_bug.cgi?id=433706) (Arjen Hiemstra, Plasma 5.21.3)

Spectacle’s «Rectangular Region» mode [once again works in the Plasma Wayland session](https://bugs.kde.org/show_bug.cgi?id=432260) (Влад Загородний, Plasma 5.22)

When using Dropbox and setting its System Tray item to be «always hidden», [it now remembers this state after the computer is rebooted](https://bugs.kde.org/show_bug.cgi?id=378910) (Konrad Materka, Plasma 5.22)

File dialogs [now add the correct filename extension while saving in the particular case when the document’s filename already ends with a period](https://bugs.kde.org/show_bug.cgi?id=431638) (Robert Hoffman, Frameworks 5.81)

Headsets with an integrated «Play/Pause» button [now do what you expect every time that button is pressed, not only every second time](https://bugs.kde.org/show_bug.cgi?id=403636) (David Redondo, Frameworks 5.81)

Panel margins [no longer change when compositing is disabled](https://bugs.kde.org/show_bug.cgi?id=430622) (Niccolò Venerandi, Frameworks 5.81)

## User Interface Improvements

Konsole [once again exposes the default profile in the UI](https://bugs.kde.org/show_bug.cgi?id=433210) (Ahmad Samir, Konsole 21.04)

Okular’s [support for FictionBook files has been enhanced to support <annotation> and <subtitle> tags](https://bugs.kde.org/show_bug.cgi?id=340134) (Юрий Чорноиван, Okular 21.04)

After moving or copying a file, [the notification now indicates which app will open the file if you click on the «Open» button](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/575) (Kai Uwe Broulik, Plasma 5.22):

![5](https://pointieststick.files.wordpress.com/2021/03/open-with-gwenview-from-notification.png?w=778)

The Plasma Vaults applet [now offers a standalone «Show in File Manager» action that you can activate for open vaults to easily jump right there](https://invent.kde.org/plasma/plasma-vault/-/merge_requests/10) (Nate Graham, Plasma 5.22):

![6](https://pointieststick.files.wordpress.com/2021/03/show-in-flie-manager-vaults-action.png?w=1024)

Using Discover to update your Nvidia drivers (or any other packages with a license agreement) [no longer prompts you to re-accept the license agreement unless it has actually changed](https://bugs.kde.org/show_bug.cgi?id=431871) (Aleix Pol Gonzalez, Plasma 5.22)

Sticky Note widgets [now ask for confirmation when you try to delete a note, but only when the note has real content in it–not when it’s empty or when you just created it from clipboard content](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/59) (Nate Graham, Plasma 5.22):

<https://i.imgur.com/O59ZVBs.mp4>

<https://i.imgur.com/iyl229d.mp4>

The new Plasma System Monitor app [now remembers the way you left your table columns and sidebars when you quit and re-launch it](https://invent.kde.org/plasma/libksysguard/-/merge_requests/136) (Arjen Hiemstra, Plasma 5.22)

Breeze icons that depict locked or unlocked states [now follow the same visual convention of locked icons having a filled body and unlocked icons having a hollow body](https://bugs.kde.org/show_bug.cgi?id=244542) (Nate Graham, Frameworks 5.81):

![7](https://pointieststick.files.wordpress.com/2021/03/light-icons.png?w=1024) ## Web presence

Check out episode four of Niccolò’s series on how to create a Plasma theme:

<https://www.youtube.com/watch?v=QWomFi0WGic>

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Андреев](https://vk.com/ilyandl)  
_Источник:_ https://pointieststick.com/2021/03/12/this-week-in-kde-elisa-grows-up/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: convergent → адаптивный -->
<!-- 💡: icons → значки -->
<!-- 💡: location → расположение -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: venerable → древний -->
